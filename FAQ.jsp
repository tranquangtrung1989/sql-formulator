<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="Format.css" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" href="Image/favicon.ico">
<title>FAQ</title>
</head>
<body>

 <div id="nav">
 <ul>
<li><a href="Homepage.jsp">Home page</a></li>
<li><a href="AboutUs.jsp">About us</a></li>
<li><a href="Program.jsp">Program</a></li>
<li><a href="FAQ.jsp">FAQ</a></li>
</ul>
</div>

<div id="container1">

<p><br/> Things you need to know about <b>SQL Formulator</b> can be found in here:</p>

<h4><img src="Image/QuestionMark.jpg" alt="QuestionMark"> What is SQL Formulator?</h4>
<p><b><font color="#FF0000">Answer:</font></b> This is an application which allows users to view diagrams from predefined databases in the system. The diagrams will display the characteristics 
of database in more friendly user way. In addition, <b>SQL Formulator</b> supports users in creating the queries by interacting with diagrams.
It helps even the inexperienced users to work with database easily.</p>

<h4><img src="Image/QuestionMark.jpg" alt="QuestionMark"> Why was SQL Formulator created?</h4>
<p><b><font color="#FF0000">Answer:</font> SQL Formulator</b> is created as a Final Year Project to fulfill the requirement for the Bachelor(Hons) of Technology Business Information System
in University Technology Petronas, Perak, Malaysia. </p>

<h4><img src="Image/QuestionMark.jpg" alt="QuestionMark"> How to use SQL Formulator?</h4>
<p><b><font color="#FF0000">Answer:</font></b> In order to use <b>SQL Formulator</b>, users firstly select one database from the list. The diagram will be automatically created
for users to view. The diagram is able to be interacted, thus users may select the attributes they want to query. 
The application will generate the SQL queries based on user's activity. </p>

<h4><img src="Image/QuestionMark.jpg" alt="QuestionMark"> Where should I contact with if I have problem with SQL Formulator?</h4>
<p><b><font color="#FF0000">Answer:</font></b> If you have any inquiries, you can find me at <a href="mailto: lengocha89nt@gmail.com">lengocha89nt@gmail.com.</a>.
Your comments are highly appreciated.  </p>

<h4><img src="Image/QuestionMark.jpg" alt="QuestionMark"> Can I upload my own database so that the application will create the diagram?</h4>
<p><b><font color="#FF0000">Answer:</font></b> For the time being, this application can support only the predefined database in the system.
 It will be continued developing to automatically generate diagrams from imported databases. </p>

<p>Can't find the answer to your question? Feel free to<a href="mailto: lengocha89nt@gmail.com"> contact us</a>, we are always ready to assist you with any questions you may have in mind</p>
</div>

<div id="nav1">
    <ul>
<li><a href="Homepage.jsp">Home page</a></li>
<li><a href="AboutUs.jsp">About us</a></li>
<li><a href="Program.jsp">Program</a></li>
<li><a href="FAQ.jsp">FAQ</a></li>
</ul>
</div>
    &copy;<b> Copyright 2012. All rights reserved.</b><br />
    
</body>
</html>