<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import ="conn.SqlQuery, java.util.List,java.lang.Math,ObjectClass.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%String dbName=request.getParameter("dbName"); %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="Format.css" rel="stylesheet" type="text/css"/>
<link href="css/small_windows.css" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" href="Image/favicon.ico">
<title>Main Program</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.TableName').click(function(event) {
			event.preventDefault();
			$.ajaxSetup({ cache: false });
			$.ajax({url:"ajax/getTblAttr.jsp?dbName=<%=dbName%>&tblName="+$.trim($(this).attr("Name")),success:function(result){
				
				   $("#boxes").html(result);
				   $('.AttributeName').click(function(event) {
						event.preventDefault();
						$("#sqlStatements").val("Select "+$(this).attr("id") +" from "+$(this).attr("tblNme"));
			    	});
				   $('.TableNameSQL').click(function(event){
					   event.preventDefault();
					   $("#sqlStatements").val("Select * from "+$(this).attr("id"));
				   });
				   
				   $("#submitSql").click(function(){
					   if($.trim($("#sqlStatements").val()).length>0)
					   {
						   $.ajaxSetup({ cache: false });
							$.ajax({url:"ajax/getSqlRs.jsp?dbNme="+$(this).attr("name")+"&sql="+$.trim($("#sqlStatements").val()),success:function(result){
								$("#sqlSelectResult").html(result);
							}});
							
					   }
				   });
				   
				   $(document).keydown(function (e) {// truong hop an phim shift
					    if (e.keyCode == 16) {
					    	$('.AttributeName').click(function(event) {
								event.preventDefault();
								var sql="Select "+$(this).attr("id") ;
								var atStr=$("#sqlStatements").val().trim();
								if(atStr.length()>0&&atStr.indexOf("*")<0){
								    var oldStr=atStr.subStr(atStr.indexOf(" "), atStr.indexOf("from"));
								    var attrs= oldStr.splilt(",");
								    for(var i=0;i<attrs.length;i++){
								    	if(attrs[i].trim()!=$(this).attr("id").trim())
								    		sql+=","+attrs[i].trim();
								    }
								}
								
								sql+=+" from "+$(this).attr("tblNme");
								$("#sqlStatements").val(sql);
					    	});
					    }
					});
	    }});
    	});
		   $('.connectLine').click(function(event) {
				event.preventDefault();
				$.ajaxSetup({ cache: false });
				$.ajax({url:"ajax/getForeignKey.jsp?dbNme="+$(this).attr("dbName")+"&tbl1="+$(this).attr("tbl1")+"&tbl2="+$(this).attr("tbl2"),success:function(result){
					   $("#middleblock").html(result);
						
		    			//Get the A tag
		        		var id = "#dialog";
		        		//Get the screen height and width
		        		var maskHeight = $(document).height();
		        		var maskWidth = $(window).width();
		        		//Set heigth and width to mask to fill up the whole screen
		        		$('#mask').css({'width':maskWidth,'height':maskHeight});
		        		
		        		//transition effect		
		        		$('#mask').fadeIn(1000);	
		        		$('#mask').fadeTo("slow",0.8);	
		        	
		        		//Get the window height and width
		        		var winH = $(window).height();
		        		var winW = $(window).width();
		                      
		        		//Set the popup window to center
		        		$(id).css('width',  "900px");
		        		$(id).css('height', "500px");
		        		$(id).css('top',  winH/2-$(id).height()/2);
		        		$(id).css('left', winW/2-$(id).width()/2);
		        	
		        		//transition effect
		        		$(id).fadeIn(1000);
	        		}});
	    	});
			
			$('#mask').click(function () {
	    		$(this).hide();
	    		$('.window').hide();
	    		$("#warnSms").html("&nbsp;");
	    	});			

	    	$(window).resize(function () {
	    	 
	     		var box = $('#box .window');
	     
	            //Get the screen height and width
	            var maskHeight = $(document).height();
	            var maskWidth = $(window).width();
	          
	            //Set height and width to mask to fill up the whole screen
	            $('#mask').css({'width':maskWidth,'height':maskHeight});
	                   
	            //Get the window height and width
	            var winH = $(window).height();
	            var winW = $(window).width();

	            //Set the popup window to center
	            box.css('top',  winH/2 - box.height()/2);
	            box.css('left', winW/2 - box.width()/2);
	    	 
	    	});
	});
</script>
</head>

<body>
 <div id="nav">
 <ul>
<li><a href="Homepage.jsp">Home page</a></li>
<li><a href="AboutUs.jsp">About us</a></li>
<li><a href="Program.jsp">Program</a></li>
<li><a href="FAQ.jsp">FAQ</a></li>
</ul>
</div>

<div id="container">
<%
if(dbName==null){

String sql="SELECT distinct TABLE_SCHEMA from INFORMATION_SCHEMA.COLUMNS "+
"where TABLE_SCHEMA !='information_schema' and TABLE_SCHEMA !='mysql' "+
"and TABLE_SCHEMA !='performance_schema' "+
"and TABLE_SCHEMA !='phpmyadmin' "+
"and TABLE_SCHEMA !='webauth';";
List<String[]> result=SqlQuery.sqlSelect(sql);
%>
	<div id="dtbs_store">
	<p>Select the database you want to work with:</p>
	<ul>
	<%for(int i=0;i<result.size();i++){ %>
	<li><a href="?dbName=<%=result.get(i)[0]%>"><%=result.get(i)[0] %></a></li>
	<%} %>
	</ul>
	 &nbsp;
	</div>
<%}else{

String sql="SELECT distinct TABLE_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='"+dbName+"'";
List<String[]> result=SqlQuery.sqlSelect(sql);
TableName draw=new TableName();
String drawtb="";
TableName[] objectList=new 	TableName[result.size()];
	int ini_centerX=400;//Initial postiion for the first table
	int ini_centerY=250;//Initial postiion for the first table
	int height=120;
	int width=120;
	int x_distance = 120;
	int y_distance = 100;
	
	int up=objectList.length/2;
	if(objectList.length%2==1) up++;
	
	for(int i=0;i<up; i++)
	{
		objectList[i]=new TableName();
		objectList[i].centerX= ini_centerX+i*(x_distance+width);
		objectList[i].centerY= ini_centerY;
	}
	for(int i=up;i<objectList.length; i++)
	{
		objectList[i]=new TableName();
		objectList[i].centerX= ini_centerX+(width+x_distance)/2	+(i-(int) Math.ceil((float)objectList.length/2))*(width+x_distance);
		objectList[i].centerY= ini_centerY+height+y_distance;
	}
	
	for (int i=0;i<objectList.length;i++)
	{
		String Name= result.get(i)[0];
		drawtb+=objectList[i].drawObject(objectList[i].centerX,objectList[i].centerY,Name);
	}
	String veLine="";
	for(int i=0;i<objectList.length;i++){
		String Name=objectList[i].tblName;
		String FindRelationship= "SELECT constraint_name,column_name,referenced_table_name, referenced_column_name  FROM information_schema.KEY_COLUMN_USAGE K where Constraint_schema='"+dbName+"' and table_name='"+Name+"' and constraint_name !='PRIMARY'";
		List<String[]> rel_rs=SqlQuery.sqlSelect(FindRelationship);
		for(int j=0;j<objectList.length;j++){
			if(j!=i){
				boolean isConnect=false;
				for(int k=0;k<rel_rs.size();k++)
					if(objectList[j].tblName.equals(rel_rs.get(k)[2]))
						isConnect=true;
				if(isConnect)
				    veLine+=DrawLine(objectList[i],objectList[j], objectList[i].tblName, objectList[j].tblName, dbName);
			}
		}
	}
	%>

<p>Select the table you want to work with:</p>
<%//String veLine= DrawLine(objectList[0],objectList[1]);%>
<%=drawtb %>
<%=veLine %>

<%} 
%>

<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
<div id="boxes">
</div>
</div>


<div id="nav1">
    <ul>
<li><a href="Homepage.jsp">Home page</a></li>
<li><a href="AboutUs.jsp">About us</a></li>
<li><a href="Program.jsp">Program</a></li>
<li><a href="FAQ.jsp">FAQ</a></li>
</ul>
</div>
    &copy;<b> Copyright 2012. All rights reserved.</b><br />
<div id="box">
		<div id="dialog" class="window">
			<div id="middleblock">&nbsp;
			</div>
		</div>
</div>
			<!-- Mask to cover the whole screen -->
<div id="mask"></div>
</body>
</html>


<%! public String DrawLine(DiagramObject d1, DiagramObject d2, String tblNme1, String tblNme2, String dbName)	//function to draw lines between two specific objects
{
	String draw="";
 	String position= "absolute";
 	String setup ="-moz-transform-origin: 0%";
	int height=3;
	String z_index="z-index:0";
	double angle=0;
	String background= "#000";	
	int x_element=0;
	int y_element=0;
	double width=0;
	width= Math.sqrt(Math.pow((d1.centerX-d2.centerX),2)+Math.pow((d1.centerY-d2.centerY),2));
	x_element=d1.centerX;
	y_element=d1.centerY;
	if(d1.centerX<d2.centerX)
	{
		if(d1.centerY<d2.centerY)
		{
			angle=(Math.asin((d2.centerY-d1.centerY)/width))*180/Math.PI;
		}
		else
		{	
			angle=((Math.asin((d1.centerY-d2.centerY)/width))*180/Math.PI)*-1;
		}
	}
	else 
	{
		if(d1.centerY<d2.centerY)
		{
			angle=180-((Math.asin((d2.centerY-d1.centerY)/width))*180/Math.PI);
		}
		else
		{
			angle=((Math.asin((d1.centerY-d2.centerY)/width))*180/Math.PI)-180;
		}
	}
	draw="<div dbName='"+dbName+"' tbl1='"+tblNme1+"' tbl2='"+tblNme2+"' class='connectLine' style='cursor:pointer;position:"+position+";"+setup+";height: "+height+"px;"+z_index+";background:"+background+";top:"+y_element+"px;left:"+x_element+"px;width:"+width+"px;-moz-transform:rotate("+angle+"deg);'></div>";
	return draw;
}	

%>
