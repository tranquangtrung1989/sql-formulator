<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ page import ="conn.SqlQuery, java.util.List"%>
 <script type="text/javascript" src="js/jquery.js"></script>
 <script type="text/javascript">
function showIt() {
  var matchedElements = document.getElementsByClassName("line");
  for (var i=0;i<matchedElements.length;i++) {
	  matchedElements[i].style.visibility = "visible";
  }
}
setTimeout("showIt()", 6000); 
setTimeout(function(){	$("#link").show();$("#link").animate({width:"88px"},1000);},7000);
</script>
<%
String tbl1=request.getParameter("tbl1").trim();
String tbl2=request.getParameter("tbl2").trim();
String dbName=request.getParameter("dbNme");

String sql="SELECT column_name, referenced_column_name "+
"FROM information_schema.KEY_COLUMN_USAGE K "+
"where Constraint_schema='"+dbName+"' "+
"and table_name='"+tbl1+"' "+
"and constraint_name !='PRIMARY' "+
"and referenced_table_name='"+tbl2+"'";
String key1="", key2="";
List<String[]> rs=SqlQuery.sqlSelect(sql);
if(rs.size()==0){
	String tmp=tbl2;
	tbl2=tbl1;
	tbl1=tmp;
	sql="SELECT column_name, referenced_column_name "+
			"FROM information_schema.KEY_COLUMN_USAGE K "+
			"where Constraint_schema='"+dbName+"' "+
			"and table_name='"+tbl1+"' "+
			"and constraint_name !='PRIMARY' "+
			"and referenced_table_name='"+tbl2+"'";
	rs=SqlQuery.sqlSelect(sql);
}
key1=rs.get(0)[0];
key2=rs.get(0)[1];
%>

<div id="tablebg" 	style="top:255px; left:150px;"><p><%=tbl1 %></p></div>
<div id="tablebg"	style="top:255px; left:650px;"><p><%=tbl2 %></p></div>
<div id="ellipseDiv"	style="top:129px; left:300px;background-color:#80CC00;"><p><%=key1 %></p></div>
<div id="ellipseDiv1"	style="top:129px; left:500px;background-color:#CC00FF;"><p><%=key2 %></p></div>
<div class="line" style="top:267px;left:250px;width:118px;-moz-transform:rotate(-45deg);visibility: hidden;"></div>
<div class="line" style="top:180px; left:590px;width:123px;-moz-transform:rotate(45deg);visibility: hidden;"></div>
<div id="link" style="top:155px; margin-left:413px"></div>  


<p1>This is the relationship between <%=tbl1 %> and <%=tbl2 %> in the database <%=dbName %></p1>
