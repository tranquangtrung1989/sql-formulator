<%@ page import ="conn.SqlQuery, java.util.List,java.lang.Math, ObjectClass.*" %>
<div id="dialog" class="window">
			<div id="middleblock">&nbsp;

<%
			//List<String[]> result;
			String tblName=request.getParameter("tblName");
			String dbName=request.getParameter("dbName");

			
			TableName tb= new TableName(); 
			String drawCenterTable= tb. drawObject(850,850,tblName);%>
			<p style="margin-left: 20px;">There are attributes of this table:</p>
			<%=drawCenterTable%>
			
			<%if(tblName!=null&&dbName!=null){
				String sql1="SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where "+
							"TABLE_NAME='"+tblName+"' and TABLE_SCHEMA='"+dbName+"'";
				List<String[]> rs=SqlQuery.sqlSelect(sql1);
				AttributeName draw=new AttributeName();
				String drawAtt="";
				AttributeName[] objectList=new AttributeName[rs.size()];
						
				
				String[] attrColor= {"#CC0000","#FF0066","#660099","#006600","#3300FF"};
				String[] usedColor= new String[5];
				int m=0;
				int k=0;
				int centerX_first=850;int centerY_first=650; int radius=200; //Initial default value to set the position of attribute
				int negativeFlag=1;
				int numOfAngle=0;
				int t=0;
				
				if ((rs.size() % 2 == 0) && (rs.size()< 8)) {t=1;}
				
				for(int j=0;j<rs.size();j++){
					objectList[j]=new AttributeName();
					usedColor[k] = attrColor[m];
					m=m+1;
					k=k+1;
					if(m>=5){m=0;k=0;}
					
					if(j%2==0)  
					{
					       if(t==0) {numOfAngle = j; negativeFlag=1;}
					       else {numOfAngle = j+2; negativeFlag=-1;};
					}
					else
					{
					       if(t==0) {numOfAngle = j+1; negativeFlag=-1;}
					       else {numOfAngle =j+1; negativeFlag=1;};
					}
					objectList[j].centerX= (int) (centerX_first+ Math.sin(negativeFlag*((22.5*numOfAngle)*Math.PI/180))*radius)	;
					objectList[j].centerY= (int) (centerY_first+ (radius-radius* Math.cos((22.5*numOfAngle)*Math.PI/180)))	;
					String name= rs.get(j)[0];
					drawAtt+=objectList[j].drawObject(objectList[j].centerX,objectList[j].centerY,name,attrColor[m],tblName);			
					String line= DrawLine(tb,objectList[j]);
					%><%=line %>
				<%}%>	
				<%=drawAtt%>		
<%}%>

<textarea id="sqlStatements" name="sql" rows="3" cols="50" style="margin-top: 50px;">

</textarea><br/>
<button id="submitSql" name="<%=dbName%>">Submit</button>
</div>
<div id="sqlSelectResult"></div>
</div>

<%! public String DrawLine(DiagramObject d1, DiagramObject d2)	//function to draw lines between two specific objects
{
	String draw="";
 	String position= "absolute";
 	String setup ="-moz-transform-origin: 0%";
	int height=3;
	String z_index="z-index:0";
	double angle=0;
	String background= "#000";	
	int x_element=0;
	int y_element=0;
	double width=0;
	width= Math.sqrt(Math.pow((d1.centerX-d2.centerX),2)+Math.pow((d1.centerY-d2.centerY),2));
	x_element=d1.centerX;
	y_element=d1.centerY;
	if(d1.centerX<d2.centerX)
	{
		if(d1.centerY<d2.centerY)
		{
			angle=(Math.asin((d2.centerY-d1.centerY)/width))*180/Math.PI;
		}
		else
		{	
			angle=((Math.asin((d1.centerY-d2.centerY)/width))*180/Math.PI)*-1;
		}
	}
	else 
	{
		if(d1.centerY<d2.centerY)
		{
			angle=180-((Math.asin((d2.centerY-d1.centerY)/width))*180/Math.PI);
		}
		else
		{
			angle=((Math.asin((d1.centerY-d2.centerY)/width))*180/Math.PI)-180;
		}
	}
	draw="<div style='position:"+position+";"+setup+";height: "+height+"px;"+z_index+";background:"+background+";top:"+y_element+"px;left:"+x_element+"px;width:"+width+"px;-moz-transform:rotate("+angle+"deg);'></div>";
	return draw;
}	

%>
	

	
