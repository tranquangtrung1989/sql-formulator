package conn;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

public class Conn {
	private final DataSource DATA_SOURCE;
	private String url="jdbc:mysql://localhost:3306/mysql";
	private String userName="root";
	private String password="";
    Conn() {
        PoolProperties p = new PoolProperties();
        p.setUrl(url);
        p.setDriverClassName("com.mysql.jdbc.Driver");
        p.setUsername(userName);
        p.setPassword(password);
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setMaxActive(100);
        p.setInitialSize(3);
        p.setMaxIdle(5);
        p.setMinIdle(0);
        p.setMaxAge(1000);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
          "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        this.DATA_SOURCE = new DataSource();
        this.DATA_SOURCE.setPoolProperties(p);
    }
    
    public DataSource getDataSource(){
    	return this.DATA_SOURCE;
    }
}