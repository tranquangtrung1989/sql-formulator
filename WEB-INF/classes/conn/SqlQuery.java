package conn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.tomcat.jdbc.pool.DataSource;

public class SqlQuery {
	public static List<String[]> sqlSelect(String sql) throws SQLException{
		System.out.println("Execute = "+sql);
		Conn conn=new Conn();
		DataSource datasource=conn.getDataSource();
		Connection con = null;
		Statement st=null;
		ResultSet rs=null;
		List<String[]> result=new ArrayList<String[]>();
        try {
          con = datasource.getConnection();
          st = con.createStatement();
          rs = st.executeQuery(sql);
          ResultSetMetaData rsmd = rs.getMetaData();
          int cnt = rsmd.getColumnCount();
          while (rs.next()) {
        	  String[] colResult = new String[cnt];
				for (int i = 0; i < cnt; i++) {
					colResult[i] = rs.getString(i+1);
				}
				result.add(colResult);
          }
          rs.close();
          st.close();
          con.close();
        } 
        catch(Exception e)
		{e.printStackTrace();}
		finally
		{
			rs.close();
	        st.close();
	        con.close();
		}
        return result;
	}
	
	public static void sqlExe(String sql) throws SQLException
	{
		System.out.println("Execute = "+sql);
		Conn conn=new Conn();
		DataSource datasource=conn.getDataSource();
		PreparedStatement ps=null;
		Connection con = null;
		try{
			con = datasource.getConnection();
			ps=con.prepareStatement(sql);
			ps.executeUpdate();
			ps.close();
			con.close();
		}
		catch(Exception e)
		{e.printStackTrace();}
		finally
		{
			ps.close();
			con.close();
		}
	}
}