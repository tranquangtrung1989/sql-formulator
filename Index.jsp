<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- link href="Format.css" rel="stylesheet" type="text/css"/-->
<link rel="shortcut icon" href="Image/favicon.ico">
<title>Final Year Project</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	setTimeout(function(){ location.href="Homepage.jsp"; }, 3000);

});
</script>
 <style type="text/css">
       html, body {
        height: 100%;
        width: 100%;
        padding: 0;
        margin: 0;
      }

      #background-image {
        z-index: -999;
        min-height: 580px;
        min-width: 935px;
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
      }

      #wrapper {
        position: relative;
        width: 935px;
        min-height: 580px;
        margin: 100px auto;
        color: #333;
      }
    </style>
    
</head>
<body><!-- style = "background-image: url('Image/utp1.jpg');background-repeat:no-repeat;" -->
	<img alt="full screen background image" src="Image/utp1.jpg" id="background-image">
	<div id="wrapper">&nbsp;

	</div>
</body>
</html>