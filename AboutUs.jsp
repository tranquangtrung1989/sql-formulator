<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="Format.css" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" href="Image/favicon.ico">
<title>About Us</title>
</head>

<body>

 <div id="nav">
 <ul>
<li><a href="Homepage.jsp">Home page</a></li>
<li><a href="AboutUs.jsp">About us</a></li>
<li><a href="Program.jsp">Program</a></li>
<li><a href="FAQ.jsp">FAQ</a></li>
</ul>
</div>
  
 <div id="container1">
<p><br/><br/><b> SQL Formulator</b> is an application developed by Le Ngoc Ha, Final year student in University Technology PETRONAS.<br/><br/>
The goal for developing this application is to fulfill the requirement of the University for achieving the Bachelor(Hons) of Technology in Business Information System<br/><br/>
 With that purpose, this application is developed within the scope of two semesters(28 weeks)<br/><br/>
 If you have any ideas or suggestions about my application, please do not hesitate to <a href="mailto: lengocha89nt@gmail.com"> contact us</a>. You inspire me to work better and harder to develop this program. <br/></p>
</div>

<div id="nav1">
    <ul>
<li><a href="Homepage.jsp">Home page</a></li>
<li><a href="AboutUs.jsp">About us</a></li>
<li><a href="Program.jsp">Program</a></li>
<li><a href="FAQ.jsp">FAQ</a></li>
</ul>
</div>
       &copy;<b> Copyright 2012. All rights reserved.</b><br />
    
</body>
</html>