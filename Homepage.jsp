<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="Format.css" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" href="Image/favicon.ico">
<title>Final Year Project</title>
</head>
<body>
    <div id="nav">
    <ul>
<li><a href="Homepage.jsp">Home page</a></li>
<li><a href="AboutUs.jsp">About us</a></li>
<li><a href="Program.jsp">Program</a></li>
<li><a href="FAQ.jsp">FAQ</a></li>
</ul>
    </div>
<div id="container1">
<h1>Welcome to SQL Formulator!</h1>
<h3>What is SQL Formulator? </h3>
<p> Very simple, it is an application which helps generate diagram for users and allows them to interact with the diagram for the purpose of creating the equivalent SQL queries.
It is a <b>visual SQL queries builder</b> that allows users to build queries in a more creative and easy way.</p>
<table border="0">
<tr>
<td><img src="Image/Logo.PNG" alt="SQL Formulator"> </td>

<td><p><b>SQL Formulator</b> is a user-friendly application which assist users in interacting with database. 
You are not required to be an expert in database to deal with it. <b>SQL Formulator</b> can make database become an easy and fun work for even an inexperienced user.
Now,let's start discovering <b>SQL Formulator</b>.</p></td>
</tr>
</table>
</div>

<div id="nav1">
    <ul>
<li><a href="Homepage.jsp">Home page</a></li>
<li><a href="AboutUs.jsp">About us</a></li>
<li><a href="Program.jsp">Program</a></li>
<li><a href="FAQ.jsp">FAQ</a></li>
</ul>
</div>
     &copy;<b> Copyright 2012. All rights reserved.</b><br />
</body>
</html>